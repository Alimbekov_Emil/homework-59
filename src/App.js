import "./App.css";
import FormBlock from "./Container/FormBlock/FormBlock";

const App = () => {
  return (
    <div className="App">
      <FormBlock />
    </div>
  );
};

export default App;
