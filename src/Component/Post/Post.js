import React, { Component } from "react";

class Post extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.value !== this.props.value;
  }

  render() {
    console.log("Updated");

    return (
      <div>
        <input
          value={this.props.value}
          onChange={this.props.change}
          type="text"
        />
        <button onClick={this.props.delete}>X</button>
      </div>
    );
  }
}

export default Post;
