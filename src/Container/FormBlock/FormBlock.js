import React, { useState } from "react";
import Task1 from "../Task1/Task1";
import Task2 from "../Task2/Task2";

const FormBlock = () => {
  const [task, setTask] = useState("task1");

  const onRadionChange = (e) => {
    setTask(e.target.value);
  };

  return (
    <div className="FormBlock">
      Домашнее задание
      <p>
        <input
          type="radio"
          name="options"
          value="task1"
          onChange={onRadionChange}
          checked={task === "task1"}
        />
        1.Задание
      </p>
      <p>
        <input
          type="radio"
          name="options"
          value="task2"
          onChange={onRadionChange}
          checked={task === "task2"}
        />
        2.Задание
      </p>
      <div>
        {task === "task1" && <Task1/>}
        {task === "task2" && <Task2/>}
      </div>
    </div>
  );
};

export default FormBlock;
