import React, { Component } from "react";
import Post from "../../Component/Post/Post";

class Task1 extends Component {
  state = {
    posts: [],
    postText: "",
  };

  handlerChange = (event) => {
    this.setState({ postText: event.target.value });
  };

  addNewPost = () => {
    if (this.state.postText !== "") {
      const posts = [...this.state.posts];
      const newPost = {
        text: this.state.postText,
      };
      posts.push(newPost);

      this.setState({
        posts,
        postText: "",
      });
    }
  };

  changePost = (event, index) => {
    const posts = [...this.state.posts];
    const post = {...posts[index]}
    console.log(post)
    post.text = event.target.value;
    posts[index] = post;

    this.setState({...this.state, posts, postText: ""})
  };

  deletePost = (index) => {
    const posts = [...this.state.posts];
    posts.splice(index, 1);
    this.setState({ posts });
  };

  render() {
    return (
      <>
        <div>
          <input
            value={this.state.postText}
            onChange={(event) => this.handlerChange(event)}
            type="text"
          />
          <button onClick={() => this.addNewPost()}>Add</button>
        </div>
        <div>
          <p>To watch list:</p>
          {this.state.posts.map((post, index) => (
            <Post
              key={index + post.text}
              value={post.text}
              change={(event) => this.changePost(event, index)}
              delete={() => this.deletePost(index)}
            />
          ))}
        </div>
      </>
    );
  }
}

export default Task1;
