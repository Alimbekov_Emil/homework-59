import React, { useEffect, useState } from "react";
import "./Task2.css";

const Task2 = (props) => {
  const [jokes, setJokes] = useState(123);
  const [newJokes, setNewJokes] = useState();

  const url = "https://api.chucknorris.io/jokes/random";

  const goNewJokes = () => {
    setNewJokes(jokes);
  };

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(url);

      if (response.ok) {
        const jokes = await response.json();
        setJokes(jokes.value);
      }
    };

    fetchData().catch(console.error);
  }, [newJokes]);

  console.log(newJokes)

  return (
    <div className="Task2">
      <button onClick={goNewJokes}>New jokes</button>
      <p>Joke from Chuck Norris: :{jokes}</p>
    </div>
  );
};

export default Task2;
